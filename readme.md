#Station : Mountain Rescue
This project is the station that will be deployed in the controlled areas to allow users to connect to the system.

##Overview
The implementation will be a server running a Wifi-P2P access point. Client application can connect to the device and use the offered services, for example

1) Download local detailed map

2) Signal for help

3) Register hiker information and time of travel.

The implementation is in golang
