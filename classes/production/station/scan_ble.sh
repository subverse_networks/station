#!/bin/bash
sudo hcitool lescan --duplicates &
last_pid=$!
cat > ./stop_scan_ble.sh <<EOL
#!/bin/bash
sudo kill $last_pid
EOL
