#!/bin/bash
pipe=/tmp/hcidump_data
if [[ ! -p $pipe ]]; then
    EXIT
fi
while true
do
    if read line <$pipe; then
        echo $line
    fi
done