#!/bin/bash
pipe=/tmp/hcidump_data
#trap "rm -f $pipe" EXIT
if [[ ! -p $pipe ]]; then
    sudo mkfifo $pipe
fi
#touch /tmp/hcidump_data
sudo hcidump --raw > /tmp/hcidump_data &
last_pid=$!
cat > ./stop_hcidump_dump.sh <<EOL
#!/bin/bash
sudo kill $last_pid
rm -f $pipe
EOL