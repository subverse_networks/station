/**
 * Created by rabbiddog on 22.09.16.
 */
import org.junit.*;
import de.tum.in.mountain_rescue.station.packet.*;
import de.tum.in.mountain_rescue.station.db.*;
import sun.awt.image.ImageWatched;

import java.util.Date;
import java.util.LinkedList;

public class DBTest {

    @Test
    public void insertDataTest()
    {
        Track track = new Track("EDD1EBEAC04E5DEFA017EBB1CB1C7CC1", "EUDEBAY00000685GFFJDK93749HSVI", new Date());
        IDatabase db = SQLiteJDBC.db();

        db.saveTrack(track);
    }

    @Test
    public void insertOldDataTest()
    {
        Date d = new Date();
        Track track = new Track("EDD1EBEAC04E5K2FA017EBB1CB1C7CC1", "AUCOBAY00000685GFFJDK93749HSVI", d);
        IDatabase db = SQLiteJDBC.db();

        db.saveNewTrack(track);

        db.saveNewTrack(track);
    }

    @Test
    public void dumpData()
    {
        IDatabase db = SQLiteJDBC.db();
        LinkedList<Track> tracks = new LinkedList<>();
        tracks.add(new Track("EDD1EBEAC04E5DEFA017EBB1CB1C7CC1", "EUDEBAY00000685GFFJDK93749HSVI", new Date()));
        tracks.add(new Track("EDD1EBEAC04E5DEFA017EBB1CB1C7CC1", "EUDEBAY00000685GFFJDK93749HSVI", new Date(152829232902342l)));
        tracks.add(new Track("70D1EBEAC04E5DEFA017EBB1CB1C8B8C", "EUDEBAY00000685GFFJDK93749HSVI", new Date()));
        tracks.add(new Track("EDD1EBEAC04E5DEFA017EBB1CB1C7CC1", "EUDEBER00000685GFFJDKHK4VJ684D", new Date()));
        tracks.add(new Track("70D1EBEAC04E5DEFA017EBB1CB1C8B8C", "EUDEBER00000685GFFJDKHK4VJ684D", new Date(1263640827314l)));
        for (Track t:tracks) {
            db.saveTrack(t);
        }
    }

    @Test
    public void getAllTracksTest()
    {
        IDatabase db = SQLiteJDBC.db();

        LinkedList<Track> tracks = db.getAllTracks();
        Assert.assertNotNull(tracks);
    }

    @Test
    public void getHikerByTagTest()
    {
        IDatabase db = SQLiteJDBC.db();

        LinkedList<Track> tracks = db.getTrackByHikerTag("EDD1EBEAC04E5DEFA017EBB1CB1C7CC1");

        Assert.assertNotNull(tracks);
        for (Track t:tracks)
        {
            Assert.assertEquals("EDD1EBEAC04E5DEFA017EBB1CB1C7CC1", t.hiker.toString());
        }
    }

    @Test
    public void getHikerByTimeTest()
    {
        IDatabase db = SQLiteJDBC.db();
        Date start = new Date(50000);
        Date end = new Date();

        LinkedList<Track> tracks = db.getTrackByTimeRange(start, end);
        Assert.assertNotNull(tracks);
        for (Track t:tracks)
        {
            Assert.assertTrue("Time did not match", (t.detected.getTime() >= start.getTime() && t.detected.getTime() <= end.getTime()));
        }
    }

    @Test
    public void getHikerByStationTest()
    {
        IDatabase db = SQLiteJDBC.db();

        LinkedList<Track> tracks = db.getTrackByStation("EUDEBAY00000685GFFJDK93749HSVI");

        Assert.assertNotNull(tracks);
        for (Track t:tracks)
        {
            Assert.assertEquals("EUDEBAY00000685GFFJDK93749HSVI", t.station.toString());
        }
    }

    @Test
    public void getHikerAtStationTest()
    {
        IDatabase db = SQLiteJDBC.db();

        LinkedList<Track> tracks = db.getTrackByHikerAtStation("EDD1EBEAC04E5DEFA017EBB1CB1C7CC1", "EUDEBAY00000685GFFJDK93749HSVI");

        Assert.assertNotNull(tracks);
        for (Track t:tracks)
        {
            Assert.assertEquals("EDD1EBEAC04E5DEFA017EBB1CB1C7CC1", t.hiker.toString());
            Assert.assertEquals("EUDEBAY00000685GFFJDK93749HSVI", t.station.toString());
        }
    }

    @Test
    public void saveNewSOSTest()
    {
        IDatabase db = SQLiteJDBC.db();
        Date d = new Date();
        SOS sos = new SOS("70D1EBEAC04E5DEFA017EBB1CB1C8B8C", "EUDEBER00000685GFFJDKHK4VJ684D", d, "help. I lost the way back\\r\\n running out of water");
        db.saveNewSOS(sos);

        db.saveNewSOS(sos);
    }
}

