/**
 * Created by rabbiddog on 21.09.16.
 */
import org.junit.*;
import de.tum.in.mountain_rescue.station.packet.*;
import de.tum.in.mountain_rescue.station.packet.marshall.*;

import java.util.Date;

public class PacketManagerTest {

    @Test
    public void encodeTrackTest()
    {
        Date d = new Date();
        Track t = new Track("EDD1EBEAC04E5DEFA017EBB1CB1C7CC1", "EUDEBAY00000685GFFJDK93749HSVI", d);

        byte[] data = TrackMarshall.encode(t);

        Assert.assertNotNull(data);
        Assert.assertEquals(data.length, 82);

        Track decoded = TrackMarshall.decode(data);

        Assert.assertEquals(t.hiker.toString(), decoded.hiker.toString());
        Assert.assertEquals(t.station, decoded.station);
        Assert.assertEquals(t.getDetectedTime(), decoded.getDetectedTime());
    }
}
