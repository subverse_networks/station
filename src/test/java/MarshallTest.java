/**
 * Created by rabbiddog on 26.09.16.
 */
import org.junit.*;
import de.tum.in.mountain_rescue.station.packet.*;
import de.tum.in.mountain_rescue.station.packet.marshall.*;

import java.util.Arrays;


public class MarshallTest {

    @Test
    public void endcodeMapResponseTest()
    {
        GetMapResponse resp = GetMapResponse.getLocalStationMap();

        Assert.assertNotNull(resp);
        Assert.assertNotNull(resp.station);
        Assert.assertNotNull(resp.map);

        byte[] data = Marshall.encode(resp);

        Assert.assertNotNull(data);
        Assert.assertEquals(resp.map.length + 31, data.length);
    }

    @Test
    public void decodeMapResponseTest()
    {
        GetMapResponse resp = GetMapResponse.getLocalStationMap();

        Assert.assertNotNull(resp);
        Assert.assertNotNull(resp.station);
        Assert.assertNotNull(resp.map);

        byte[] data = Marshall.encode(resp);

        Assert.assertNotNull(data);
        Assert.assertEquals(resp.map.length + 31, data.length);

        GetMapResponse decoded = (GetMapResponse) Marshall.decode(data);

        Assert.assertNotNull(decoded);
        Assert.assertEquals(resp.station, decoded.station);
        Assert.assertTrue(Arrays.equals(resp.map, decoded.map));
    }
}
