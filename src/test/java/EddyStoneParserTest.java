/**
 * Created by rabbiddog on 15.09.16.
 */
import de.tum.in.mountain_rescue.station.eddystone.Parse;
import org.junit.*;
import de.tum.in.mountain_rescue.station.eddystone.frames.*;

public class EddyStoneParserTest{

    @Test
    public void parseEddystoneUidValidDataTest()
    {
        String raw = "04 3E 29 02 01 00 01 C1 7C 1C CB B1 EB 1D 02 01 06 03 03 AA" +
                    " FE 15 16 AA FE 00 EB ED D1 EB EA C0 4E 5D EF A0 17 EB B1 CB" +
                    " 1C 7C C1 D0";

        Parse frameParser = Parse.getInstance();

        EddystoneFrame frame = frameParser.parseData(raw);

        Assert.assertTrue("Frame of type EddystoneUid", frame instanceof EddystoneUidFrame);


        EddystoneUidFrame uidFrame = (EddystoneUidFrame) frame;

        Assert.assertEquals("Tx power match", -21, uidFrame.txPower_dBm);
        Assert.assertEquals("Rssi match", -48, uidFrame.RSSI);
        Assert.assertEquals("Uid match", "EDD1EBEAC04E5DEFA017", uidFrame.namespace);
        Assert.assertEquals("instance match", "EBB1CB1C7CC1", uidFrame.instance);
    }
}
