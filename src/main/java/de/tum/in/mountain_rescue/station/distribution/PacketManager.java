package de.tum.in.mountain_rescue.station.distribution;

/**
 * Created by rabbiddog on 21.09.16.
 */
import de.tum.in.mountain_rescue.station.packet.marshall.*;
import de.tum.in.mountain_rescue.station.packet.*;
import fi.tkk.netlab.dtn.scampi.applib.*;
import de.tum.in.mountain_rescue.station.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.LinkedList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class PacketManager extends service implements PublishDoneCallback {

    private static PacketManager _instance;
    private AtomicBoolean _started;
    private static Logger _log;
    private Object lock;
    private AppLib APP_LIB ;
    private LinkedList<IPacketReceived> packetSubscriber;
    private LinkedList<IPeerDetected> peerSubscriber;
    private ScheduledExecutorService service_connect;
    private final static String TAG = "Class : PacketManager :-";
    private PacketManager()
    {
        _log = LogManager.getRootLogger();
        lock = new Object();
        _started = new AtomicBoolean(false);
        packetSubscriber = new LinkedList<>();
        peerSubscriber = new LinkedList<>();
        service_connect = Executors.newScheduledThreadPool(3); //for scheduling connect()
    }
    public static PacketManager getInstance()
    {
        if(null == _instance)
        {
            _instance = new PacketManager();
        }
        return _instance;
    }

    @Override
    public boolean start() {

        if(_started.get())
        {
            _log.debug(TAG + "start() service already running");
            return true;
        }
        APP_LIB = AppLib.builder().build(); //once APP_LIB is closed then same instance cannot be started again
        APP_LIB.start();
        APP_LIB.addLifecycleListener( new LifeCycleListner() );
        APP_LIB.connect();
        APP_LIB.addMessageReceivedCallback( new MessageReceiver() );
        // Subscribe to a service
        try {
            APP_LIB.subscribe( Util.getServiceName());
        }catch (InterruptedException inex)
        {
            _log.error(TAG + "start() error StackTrace : "+inex.getStackTrace()+ " Messgae : "+inex.getMessage());
            return false;
        }
        _log.debug(TAG + " service has started");
        _started.set(true);
       return true;
    }

    @Override
    public boolean stop() {
        if(!_started.get())
        {
            _log.debug(TAG + "stop() service was not running");
            return true;
        }
        /*stop the executor service*/
        service_connect.shutdownNow();
        try
        {
            _log.debug(TAG + "stop() awaiting termination");
            service_connect.awaitTermination(3, TimeUnit.SECONDS);
            _log.debug(TAG + "stop() process stopped");
        }catch (InterruptedException inex)
        {
            _log.error(TAG + "stop() stop process was interrupted");
        }
        APP_LIB.stop();
        _log.debug(TAG + "stop() service has stopped");
        _started.set(false);
        return true;
    }

    public void subscribe(IPacketReceived sub)
    {
        synchronized(lock)
        {
            _log.debug(TAG + "register() new subscriber added");
            packetSubscriber.add(sub);
        }
    }

    public void detectPeer(IPeerDetected sub)
    {
        synchronized (lock)
        {
            _log.debug(TAG + "detectPeer() new subscriber added");
            peerSubscriber.add(sub);
        }
    }
    public void sendPacket(Packet packet)
    {
        if(!_started.get())
        {
            _log.error(TAG + "sendPacket() attempt to send packet when service is not started");
            return;
        }
        SCAMPIMessage message = null;
        if(packet instanceof Track)
        {
            _log.debug(TAG + "sendPacket() received packet type Track to send");
            Track t = (Track) packet;
            message = createScampiMessage(t);
        }
        else if(packet instanceof SOS)
        {
            _log.debug(TAG + "sendPacket() received packet type SOS to send");
            SOS sos = (SOS)packet;
            message = createScampiMessage(sos);
        }
        else if(packet instanceof GetMapResponse)
        {
            _log.debug(TAG + "sendPacket() received packet of type GetMapResponse to send");
            GetMapResponse mapResp = (GetMapResponse)packet;
            message = createScampiMessage(mapResp);
        }

        try {

            APP_LIB.publish(message, Util.getServiceName(), this);
        }catch (InterruptedException inex)
        {
            _log.error(TAG + "sendPacket() error while publish. StackTrace : "+inex.getStackTrace()+ " Message : "+inex.getMessage());
        }
    }

    @Override
    public void publishDone(AppLib appLib, SCAMPIMessage scampiMessage)
    {
        _log.debug(TAG + "publishDone : "+ scampiMessage.getAppTag());
        scampiMessage.close();
    }

    private  final class LifeCycleListner
            implements AppLibLifecycleListener {

        private String lifecycle_listener = "Applib lifecycle listener ";
        @Override
        public void onConnected(String s) {
            _log.debug(TAG + lifecycle_listener + " onConnected() "+s);
        }

        @Override
        public void onDisconnected() {
            _log.debug(TAG + lifecycle_listener + "onDisconnected() attempting another connect in 30 sec");
            /*schedule another connect*/
            service_connect.schedule(new Thread(){
                public void run(){
                    APP_LIB.connect();
                }
            }, 30, TimeUnit.SECONDS); //try again in 30 sec
        }

        @Override
        public void onConnectFailed() {
            _log.debug(TAG + lifecycle_listener + "onConnectFailed() attempting another connect in 30 sec");
            /*schedule another connect*/
            service_connect.schedule(new Thread(){
                public void run(){
                    APP_LIB.connect();
                }
            }, 30, TimeUnit.SECONDS); //try again in 30 sec
        }

        @Override
        public void onStopped() {
            _log.debug(TAG + lifecycle_listener + "onStopped()");
        }
    }

    private final class MessageReceiver
            implements MessageReceivedCallback{

        @Override
        public void messageReceived(SCAMPIMessage scampiMessage, String s) {
            /*check if service match*/
            if(!s.equals(Util.getServiceName()))
            {
                _log.error(TAG + "messageReceived() service name received does not match. Received : "+s);
                scampiMessage.close();
                return;
            }
            try
            {
                byte[] buffer = scampiMessage.getBinaryBuffer("PDU");
                _log.debug(TAG +"messageReceived : "+scampiMessage.getAppTag());
                Packet p = Marshall.decode(buffer);
                if(null == p)
                {
                    _log.error(TAG + "messageReceived : decode failed. null received");
                }
                for (IPacketReceived ipr:packetSubscriber)
                {
                    ipr.processPacket(p); //bad design. subscriber can hold up the execution of this thread. Refactor later
                }

            }
            catch (IOException ioex)
            {
                _log.error(TAG + "messageReceived() error. StackTrace : "+ioex.getStackTrace()+" Message : "+ioex.getMessage());
            }
            finally{
                scampiMessage.close();
            }
        }
    }

    private final class HostDiscovery
            implements HostDiscoveryCallback{

        @Override
        public void hostDiscovered(AppLib appLib, String s, int i, long l, double v, double v1, double v2) {

            for (IPeerDetected ipd:peerSubscriber)
            {
                ipd.syncInformationBase();
            }
        }
    }

    private SCAMPIMessage createScampiMessage(Track track)
    {
        byte[] data = TrackMarshall.encode(track);
        String tag = track.hiker.toString()+track.station+track.getDetectedTime();
        SCAMPIMessage message = SCAMPIMessage.builder()
                .appTag( tag)
                .lifetime(Util.getSaveInterval(), TimeUnit.MINUTES) // random value.
                .persistent(false)
                .build();

        if(null == data)
            _log.error(TAG + "Track encoded is null");
        message.putBinary("PDU", data);
        _log.debug(TAG + "createScampiMessage() with tag : "+tag);
        return message;
    }

    private SCAMPIMessage createScampiMessage(SOS sos)
    {
        byte[] data = SOSMarshall.encode(sos);
        String tag = sos.hiker.toString()+sos.station+sos.getDetectedTime();
        SCAMPIMessage message = SCAMPIMessage.builder()
                .appTag( tag)
                .lifetime(Util.getSaveInterval(), TimeUnit.MINUTES) // random value.
                .persistent(false)
                .build();

        if(null == data)
            _log.error(TAG + "SOS encoded is null");
        message.putBinary("PDU", data);
        _log.debug(TAG + "createScampiMessage() with tag : "+tag);
        return message;
    }

    private SCAMPIMessage createScampiMessage(GetMapResponse mapRsp)
    {
        byte[] data  = MapResponseMarshall.encode(mapRsp);
        String tag = mapRsp.station+"map";
        SCAMPIMessage message = SCAMPIMessage.builder()
                .appTag( tag)
                .lifetime(10, TimeUnit.MINUTES) // only send to users who requested it. So remove packet after 10min
                .persistent(false)
                .build();

        if(null == data)
            _log.error(TAG + "GetMapResponse encoded is null");
        message.putBinary("PDU", data);
        _log.debug(TAG + "createScampiMessage() with tag : "+tag);
        return message;
    }

}
