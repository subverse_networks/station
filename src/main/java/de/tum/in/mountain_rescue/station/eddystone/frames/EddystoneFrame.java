package de.tum.in.mountain_rescue.station.eddystone.frames;

/**
 * Created by rabbiddog on 15.09.16.
 */
public abstract class EddystoneFrame {

    abstract public int  geType();
    @Override
    abstract public String toString();
}
