package de.tum.in.mountain_rescue.station;

/**
 * Created by rabbiddog on 23.09.16.
 */
public interface IPeerDetected {

    void syncInformationBase();
}
