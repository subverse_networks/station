package de.tum.in.mountain_rescue.station.packet;

/**
 * Created by rabbiddog on 21.09.16.
 */
public class PacketType {
    public static byte HIKER = 101;
    public static byte SOS = 102;
    public static byte TRACK= 103;
    public static byte MAPCOMMAND = 104;
    public static byte MAPRESPONSE = 105;
}
