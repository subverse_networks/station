package de.tum.in.mountain_rescue.station.eddystone.frames;

import de.tum.in.mountain_rescue.station.Util;

/**
 * Class for parsing Eddystone-EID frames.
 *
 * @author teemuk
 */
public final class EddystoneEidFrame extends EddystoneFrame {

  //==============================================================================================//
  // Constants
  //==============================================================================================//

  //==============================================================================================//

  //==============================================================================================//
  // Instance vars
  //==============================================================================================//
  /** Calibrated TX power at 0m. */
  public final int txPower_dBm;
  /** Ephemeral ID bytes. */
  public final byte[] eid;
  //==============================================================================================//


  //==============================================================================================//
  // API
  //==============================================================================================//
  public EddystoneEidFrame(
      final int txPower_dBm,
      final byte[] eid ) {
    this.txPower_dBm = txPower_dBm;
    this.eid = eid;
  }

  /**
   * Parses an Eddystone-EID frame from the given buffer.
   *
   * @param frameBytes
   *  Service Data bytes associated with the Eddystone service UUID.
   */
  public static EddystoneEidFrame parse( final byte[] frameBytes ) {
    // Validate length. Specified to exactly 10 bytes, but accept longer frames while ignoring
    // extra bytes.
    if ( frameBytes.length < 10 ) {
      return null;
    }

    // Validate type
    if ( !isEidFrame( frameBytes ) ) {
      return null;
    }

    // TX Power
    final int txPower = frameBytes[ 1 ];

    // EID
    final byte[] eid = new byte[ 8 ];
    System.arraycopy( frameBytes, 2, eid, 0, 8 );

    return new EddystoneEidFrame( txPower, eid );
  }

  /**
   * Checks that the given frame is Eddystone-UID frame. The top 4 bits must be 0000.
   *
   * @param bytes
   *  Raw frame data.
   * @return
   *  {@code true} if the frame type of the buffer matches Eddystone-UID {@code false} otherwise.
   */
  public static boolean isEidFrame( final byte[] bytes ) {
    return ( ( ( bytes[ 0 ] & 0xff ) >> 4 ) == 3 );
  }

  @Override
  public int geType() {
    return FrameType.EID;
  }

  @Override
  public String toString() {
    return "EID = " + Util.byteArrayToHex( this.eid ) + ", txPower = " + this.txPower_dBm + "dBm";
  }
  //==============================================================================================//


  //==============================================================================================//
  // Private
  //==============================================================================================//

  //==============================================================================================//
}
