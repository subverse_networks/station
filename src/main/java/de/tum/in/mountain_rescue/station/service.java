package de.tum.in.mountain_rescue.station;

/**
 * Created by rabbiddog on 21.09.16.
 */
public abstract class service {

    public abstract boolean start();
    public abstract boolean stop();
}
