package de.tum.in.mountain_rescue.station.eddystone.frames;

/**
 * Created by rabbiddog on 24.09.16.
 */
public class FrameType {
    public static int EID = 201;
    public static int TLM = 202;
    public static int UID= 203;
    public static int URL= 204;
}
