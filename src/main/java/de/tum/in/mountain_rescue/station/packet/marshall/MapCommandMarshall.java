package de.tum.in.mountain_rescue.station.packet.marshall;

import de.tum.in.mountain_rescue.station.packet.GetMapCommand;
import de.tum.in.mountain_rescue.station.packet.PacketType;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.PooledByteBufAllocator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

/**
 * Created by rabbiddog on 26.09.16.
 */
public class MapCommandMarshall {

    private static final PooledByteBufAllocator ALLOCATOR =
            PooledByteBufAllocator.DEFAULT;
    private static final int TYPE = 1;
    private static final byte type_value = PacketType.MAPCOMMAND;
    private static Logger _log = LogManager.getRootLogger();
    private final static String TAG = "Class : MapCommandMarshall :-";


    public static GetMapCommand decode(byte[] data)
    {
        checkNotNull(data);
        ByteBuf buffer = null;
        try
        {
            buffer = ALLOCATOR.buffer(data.length);
            buffer.writeBytes(data);
            byte type = buffer.readByte();
            checkState(type == type_value);

            return new GetMapCommand();

        }finally {
            if (buffer != null) {
                buffer.release();
            }
        }
    }

    public static byte[] encode(GetMapCommand cmd)
    {
        checkNotNull(cmd);
        ByteBuf buffer = null;
        try {
            buffer = ALLOCATOR.buffer(1);
            buffer.writeByte(cmd.getType());

            byte[] data = new byte[1];
            buffer.readBytes(data);

            return data;
        }finally {
            if (buffer != null) {
                buffer.release();
            }
        }
    }
}
