package de.tum.in.mountain_rescue.station.packet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.layout.StringBuilderEncoder;

import java.util.LinkedList;

/**
 * Created by rabbiddog on 21.09.16.
 */
public class Hiker extends Packet{

    private static Logger _log;
    private static String TAG = "Class : Hiker :-";
    public final String tagInfo;

    public Hiker(String tagInfo)
    {
        _log = LogManager.getRootLogger();
        if(null == tagInfo || tagInfo.equals(""))
        {
            _log.error(TAG + " constructor() taginfo is null or empty");
            throw  new IllegalArgumentException("Hiker taginfo must be a non empty string");
        }
        if(tagInfo.length() != 32)
        {
            _log.error(TAG + " constructor() taginfo is not string length 32");
            throw  new IllegalArgumentException("Hiker taginfo must be 32 characters long");
        }
        this.tagInfo = tagInfo;
    }
    public Hiker(String UId, String Instance)
    {
        _log = LogManager.getRootLogger();
        if(null == UId || UId.equals("") || null == Instance || Instance.equals(""))
        {
            _log.error(TAG + " constructor() UId or Instance is null or empty");
            throw  new IllegalArgumentException("Hiker UId and Instance must be a non empty string");
        }

        this.tagInfo = UId+Instance;
        if(this.tagInfo.length() != 32)
        {
            _log.error(TAG + " constructor() taginfo is not string length 32");
            throw  new IllegalArgumentException("Hiker taginfo must be 32 characters long");
        }
    }

    @Override
    public int getType() {
        return PacketType.HIKER;
    } //1 byte

    @Override
    public String toString()
    {
        return tagInfo;
    }
}
