package de.tum.in.mountain_rescue.station;

import de.tum.in.mountain_rescue.station.packet.Packet;

/**
 * Created by rabbiddog on 21.09.16.
 */
public interface IPacketReceived {

    void processPacket(Packet packet);
}
