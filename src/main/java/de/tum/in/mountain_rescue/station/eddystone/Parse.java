package de.tum.in.mountain_rescue.station.eddystone;

import de.tum.in.mountain_rescue.station.eddystone.frames.*;

import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.regex.Pattern;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.*;

/**
 * Created by rabbiddog on 14.09.16.
 */
public class Parse {

    private static Parse _instance;
    private static Logger _log;
    private final static String TAG = "Class : Parse :-";
    private final static String eddystoneFrameFormat = "0201060303AAFE";

    private void init()
    {
        _log = LogManager.getRootLogger();
    }
    private Parse()
    {}

    public static Parse getInstance()
    {
        if(null ==_instance)
        {
            _instance = new Parse();
            _instance.init();
        }
        return _instance;
    }

    public EddystoneFrame parseData(String data)
    {
        _log.debug(TAG + "parseData() data received " + data);
        try{
            String hex[] = data.split(" ");
        /*verify that it is infact a BLE signal*/
            if(!isEddyStone(hex))
            {
                _log.debug(TAG + "frame pattern does not match. Discarding");
                return null;
            }
            if(hex[25].equals("00"))
            {
                return EddystoneUidFrame.parse(hex);
            }
        /*else if(hex[25].equals("10"))
        {
            return EddystoneUrlFrame.parse(hex);
        }
        else if(hex[25].equals("20"))
        {
            return EddystoneTlmFrame.parse(hex);
        }*/
            else
                return null;
        }catch (Exception ex)
        {
            _log.error(TAG + "parseData() error. StackTrace :" +ex.getStackTrace()+ " Message :"+ex.getMessage());
            return null;
        }

    }

    public boolean isEddyStone(String[] frame)
    {
        if(frame.length < 44)
        {
            _log.debug(TAG + "frame length is less than expected(44). Discarding");
            return false;
        }
        /*search for pattern*/
        String original = String.join("",Arrays.copyOfRange(frame, 14, 21));

        return eddystoneFrameFormat.equals(original);
    }
}
