package de.tum.in.mountain_rescue.station;

import de.tum.in.mountain_rescue.station.db.IDatabase;
import de.tum.in.mountain_rescue.station.db.SQLiteJDBC;
import de.tum.in.mountain_rescue.station.distribution.PacketManager;
import de.tum.in.mountain_rescue.station.eddystone.frames.EddystoneFrame;
import de.tum.in.mountain_rescue.station.eddystone.frames.EddystoneUidFrame;
import de.tum.in.mountain_rescue.station.eddystone.frames.FrameType;
import de.tum.in.mountain_rescue.station.packet.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Date;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.*;

/**
 * Created by rabbiddog on 19.09.16.
 */
public class Scanner extends service implements IEddyStoneDetected, IPacketReceived, IPeerDetected {

    private static Logger _log;
    private final String TAG = "Class : Scanner :-";
    private ConcurrentLinkedQueue<EddystoneFrame> pendingFrames;
    private ConcurrentLinkedQueue<Packet> pendingPackets;
    private ScheduledExecutorService thread_service;
    private IDatabase db;

    public Scanner()
    {
        _log = LogManager.getRootLogger();
        pendingFrames = new ConcurrentLinkedQueue<>();
        pendingPackets = new ConcurrentLinkedQueue<>();
        DetectEddystoneFacade.getInstance().subscribe(this);
        PacketManager.getInstance().subscribe(this);
        thread_service = Executors.newScheduledThreadPool(4);
        db = SQLiteJDBC.db();
    }

    /*frame arrive from the bluetooth device*/
    @Override
    public void processFrame(EddystoneFrame frame) {
        _log.debug(TAG + "processFrame() frame received " + frame.toString());
        /*save frame only if they are newer than last data on hiker by inverval of save_interval
        * save_interval is defined in config.properties*/
        pendingFrames.offer(frame);
    }

    /*packet arrives from scampi middleware*/
    @Override
    public void processPacket(Packet packet) {
        _log.debug(TAG + "processPacket() packet received of type "+ packet.getType());
        pendingPackets.offer(packet);
    }

    @Override
    public boolean start() {

        DetectEddystoneFacade.getInstance().start();
        PacketManager.getInstance().start();
        PacketManager.getInstance().subscribe(this);
        PacketManager.getInstance().detectPeer(this);

        thread_service.execute(new Thread(){
            public void run(){
                startSubThreads();
            }
        });

        return true;
    }

    @Override
    public boolean stop() {
        DetectEddystoneFacade.getInstance().stop();
        PacketManager.getInstance().stop();
        thread_service.shutdownNow();
        try
        {
            _log.debug(TAG + "stop() awaiting termination");
            thread_service.awaitTermination(3, TimeUnit.SECONDS);
            _log.debug(TAG + "stop() process stopped");
            return true;
        }
        catch (InterruptedException inex)
        {
            _log.error(TAG + "stop() stop process was interrupted");
            return false;
        }
    }

    @Override
    public void syncInformationBase() {
            /*decide what to do here*/
    }

    private void startSubThreads()
    {
        _log.debug(TAG + "startSubThreads() has started");
        ScheduledFuture processPacketsFuture = thread_service.schedule(new Callable()
        {
           public Object call() throws Exception
           {
                return processPackets();
           }
        }, 0, TimeUnit.SECONDS);

        ScheduledFuture processFramesFuture = thread_service.schedule(new Callable()
        {
            public Object call() throws Exception
            {
                return processFrames();
            }
        }, 0, TimeUnit.SECONDS);

        ScheduledFuture spreadData = thread_service.schedule(new Callable() {
            public Object call() throws Exception
            {
                return spreadData();
            }
        }, 0, TimeUnit.SECONDS);

        while(true)
        {
            if(Thread.currentThread().isInterrupted())
            {
                _log.debug(TAG + "startSubThreads() was interrupted. Will shut down now");
                break;
            }
            if(processPacketsFuture.isDone() || processPacketsFuture.isCancelled())
            {
                processPacketsFuture = thread_service.schedule(new Callable() {
                    public Object call() throws Exception
                    {
                        return processPackets();
                    }
                }, 1000, TimeUnit.MILLISECONDS);
            }

            if(processFramesFuture.isDone() || processFramesFuture.isCancelled())
            {
                processFramesFuture = thread_service.schedule(new Callable() {
                    public Object call() throws Exception
                    {
                        return processFrames();
                    }
                }, 1000, TimeUnit.MILLISECONDS);
            }

            if(spreadData.isDone() || spreadData.isCancelled())
            {
                spreadData = thread_service.schedule(new Callable() {
                    public Object call() throws Exception
                    {
                        return spreadData();
                    }
                }, Util.getSaveInterval(), TimeUnit.MINUTES);
            }

        }
        _log.debug(TAG + "startSubThreads() has stopped");
    }

    /*regularly dumps all information into scampi middleware to be distributed in the environment*/
    private Boolean spreadData()
    {
        _log.debug(TAG + "spreadData() new iteration started");
        LinkedList<SOS> sosPackets = db.getAllSOS();

        for (SOS sos:sosPackets)
        {
            PacketManager.getInstance().sendPacket(sos);
        }

        LinkedList<Track> trackPackets = db.getAllTracks();
        for (Track track:trackPackets)
        {
            PacketManager.getInstance().sendPacket(track);
        }

        _log.debug(TAG + "spreadData() iteration ended");
        return true;
    }

    private Boolean processPackets()
    {
        while(true)
        {
            if(Thread.currentThread().isInterrupted())
            {
                _log.debug(TAG + "processPackets() has been interrupted. Will stop now");
                break;
            }
            Packet pck = pendingPackets.poll();
            if(null == pck)
                break;/*this thread will stop and the executor service should schedule a new processFrame thread after an inteval*/

            /*try to save all packets*/
            if(pck instanceof SOS)
            {
                /*check if SOS was generated in some other station*/
                SOS sos = (SOS)pck;
                if(sos.station.isEmpty() || sos.station.equals("000000000000000000000000000000"))
                {
                    sos.station = Util.getStationId();
                }
                _log.debug(TAG + "processPackets() received SOS message");
                db.saveNewSOS((SOS)pck);
            }
            else if(pck instanceof Track)
            {
                _log.debug(TAG +"processPackets() received Track message");
                Track trk = (Track) pck;
                if(trk.station.isEmpty() || trk.station.equals("000000000000000000000000000000"))
                {
                    trk.station = Util.getStationId();
                }
                db.saveNewTrack((Track)pck);
            }
            else if(pck instanceof GetMapCommand)
            {
                _log.debug(TAG +"processPackets() received GetMapCommand message");
                GetMapResponse resp = GetMapResponse.getLocalStationMap();
                if(null == resp)
                    _log.error(TAG + "processPackets() Map response will not be send because failed to load local map");
                else
                    PacketManager.getInstance().sendPacket(resp);
            }
            else if(pck instanceof GetMapResponse)
            {
                //do nothing.
            }
        }
        _log.debug(TAG + "processPackets() has stopped");
        return true;
    }

    private Boolean processFrames()
    {
        while(true)
        {
            if(Thread.currentThread().isInterrupted())
            {
                _log.debug(TAG + "processFrames() has been interrupted. Will stop now");
                break;
            }
            EddystoneFrame frm = pendingFrames.poll();
            if(null == frm)
                break;/*this thread will stop and the executor service should schedule a new processFrame thread after an inteval*/

            /*if there is a frame already saved in db from the past less than sca_interval ago then do not save*/
            if(frm.geType() == FrameType.UID)
            {
                EddystoneUidFrame uid = (EddystoneUidFrame)frm;
                Date d = new Date();
                Track track = new Track(uid.namespace, uid.instance, Util.getStationId(), d);
                Date pastdate = new Date(d.getTime() - (Util.getSaveInterval() * 60000));

                db.saveIfNewerTrack(track, pastdate);
            }
        }
        _log.debug(TAG + "processFrames() has stopped");
        return true;
    }
}
