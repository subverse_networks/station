package de.tum.in.mountain_rescue.station.db;

import de.tum.in.mountain_rescue.station.packet.SOS;
import de.tum.in.mountain_rescue.station.packet.Track;

import java.util.Date;
import java.util.LinkedList;

/**
 * Created by rabbiddog on 21.09.16.
 */
public interface IDatabase {

    LinkedList<Track> getAllTracks();

    LinkedList<Track> getTrackByTimeRange(Date start, Date end);

    LinkedList<Track> getTrackByHikerTag(String tag);

    LinkedList<Track> getTrackByStation(String station);

    LinkedList<Track> getTrackByHikerAtStation(String tag, String station);

    boolean saveTrack(Track track);

    boolean saveNewTrack(Track track);

    boolean saveIfNewerTrack(Track track, Date date);

    LinkedList<SOS> getAllSOS();

    LinkedList<SOS> getSOSbyStation(String station);

    boolean saveNewSOS(SOS sos);
}
