package de.tum.in.mountain_rescue.station.db;

/**
 * Created by rabbiddog on 21.09.16.
 */
import de.tum.in.mountain_rescue.station.Util;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.appender.db.jdbc.ConnectionSource;
import de.tum.in.mountain_rescue.station.packet.*;

import java.sql.*;
import java.util.*;
import java.util.Date;

public class SQLiteJDBC implements IDatabase{

    private static SQLiteJDBC _instance;
    private static Logger _log;
    private static Connection conn;
    private static String TAG = "Class : SQLiteJDBC :-";

    private SQLiteJDBC()
    {
        _log = LogManager.getRootLogger();
        /*conn = null;
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:mountainrescue.db");
        } catch ( Exception e ) {
            _log.error(TAG + "error opening database connection. StackTrace : "+e.getStackTrace()+ " Message : "+e.getMessage());
            throw new IllegalStateException("Database connection did not open");
        }
        _log.debug(TAG + "Opened database successfully");*/
    }

    public static SQLiteJDBC db()
    {
        if(null == _instance)
        {
            _instance = new SQLiteJDBC();
            _instance.initDb();
        }
        return _instance;
    }

    private void initDb()
    {
        try
        {
            String path_jar = Util.filterJarFilePath(getClass().getProtectionDomain().getCodeSource().getLocation().getFile());
            _log.debug(TAG + "initDb() path to database "+path_jar+"/mountainrescue.db");
            conn = DriverManager.getConnection("jdbc:sqlite:"+path_jar+"/mountainrescue.db");
            Statement stmt = conn.createStatement();
            String sql = "CREATE TABLE IF NOT EXISTS HIKER " +
                    "(ID INTEGER PRIMARY KEY," +
                    " TAG           TEXT    NOT NULL, " +
                    " STATION       TEXT     NOT NULL, " +
                    " TIME        BIGINT    NOT NULL)"; //time is number of minutes since epoc
            stmt.executeUpdate(sql);
            stmt.close();

            stmt = conn.createStatement();
            sql = "CREATE TABLE IF NOT EXISTS SOS " +
                    "(ID INTEGER PRIMARY KEY," +
                    " TAG           TEXT NOT NULL, " +
                    " STATION       TEXT     NOT NULL, " +
                    " TIME        BIGINT    NOT NULL, " + //time is number of minutes since epoc
                    " MESSAGE TEXT NOT NULL)";
            stmt.executeUpdate(sql);
            stmt.close();
            conn.close();

        }catch(SQLException sqlex)
        {
            _log.error(TAG + "initDb() StackTrace: "+sqlex.getStackTrace()+" Message : "+sqlex.getMessage());
        }
    }

    @Override
    public LinkedList<Track> getAllTracks()
    {
        try {
            conn = DriverManager.getConnection("jdbc:sqlite:mountainrescue.db");
            Statement stmt = conn.createStatement();
            String sql ="SELECT * FROM HIKER ;";
            ResultSet rs = stmt.executeQuery(sql);
            LinkedList<Track> tracks = new LinkedList<>();
            while ( rs.next() ) {
                String tag = rs.getString("TAG");
                String station = rs.getString("STATION");
                Date detected = new Date(rs.getLong("TIME") * 60 * 1000);

                tracks.add(new Track(tag, station, detected));
            }
            stmt.close();
            conn.close();
            return tracks;
        }catch(SQLException sqlex)
        {
            _log.error(TAG + "getAllTracks() StackTrace: "+sqlex.getStackTrace()+" Message : "+sqlex.getMessage());
            return null;
        }
    }

    @Override
    public LinkedList<Track> getTrackByTimeRange(Date start, Date end)
    {
        try {
            conn = DriverManager.getConnection("jdbc:sqlite:mountainrescue.db");
            Statement stmt = conn.createStatement();
            Long start_d = start == null ? 0 : start.getTime() / 60000;
            Long end_d = end == null ? 0 : end.getTime() / 60000;
            String sql ="SELECT * FROM HIKER " +
                    "WHERE TIME > "+start_d +" AND TIME < "+end_d+";";
            ResultSet rs = stmt.executeQuery(sql);
            LinkedList<Track> tracks = new LinkedList<>();
            while ( rs.next() ) {
                String tag = rs.getString("TAG");
                String station = rs.getString("STATION");
                Date detected = new Date(rs.getLong("TIME") * 60 * 1000);

                tracks.add(new Track(tag, station, detected));
            }
            stmt.close();
            conn.close();
            return tracks;
        }catch(SQLException sqlex)
        {
            _log.error(TAG + "getTrackByTimeRange() StackTrace: "+sqlex.getStackTrace()+" Message : "+sqlex.getMessage());
            return null;
        }
    }

    @Override
    public LinkedList<Track> getTrackByHikerTag(String tag)
    {

        try {
            conn = DriverManager.getConnection("jdbc:sqlite:mountainrescue.db");
            Statement stmt = conn.createStatement();
            String sql ="SELECT * FROM HIKER " +
                    "WHERE TAG = '"+tag+"';";
            ResultSet rs = stmt.executeQuery(sql);
            LinkedList<Track> tracks = new LinkedList<>();
            while ( rs.next() ) {
                String tag_get = rs.getString("TAG");
                String station = rs.getString("STATION");
                Date detected = new Date(rs.getLong("TIME") *60*1000);

                tracks.add(new Track(tag_get, station, detected));
            }
            stmt.close();
            conn.close();
            return tracks;
        }catch(SQLException sqlex)
        {
            _log.error(TAG + "getTrackByHikerTag() StackTrace: "+sqlex.getStackTrace()+" Message : "+sqlex.getMessage());
            return null;
        }
    }

    @Override
    public LinkedList<Track> getTrackByStation(String station) {

        try {
            conn = DriverManager.getConnection("jdbc:sqlite:mountainrescue.db");
            Statement stmt = conn.createStatement();
            String sql ="SELECT * FROM HIKER " +
                    "WHERE STATION = '"+station+"';";
            ResultSet rs = stmt.executeQuery(sql);
            LinkedList<Track> tracks = new LinkedList<>();
            while ( rs.next() ) {
                String tag = rs.getString("TAG");
                String station_get = rs.getString("STATION");
                Date detected = new Date(rs.getLong("TIME") *60 *1000);

                tracks.add(new Track(tag, station_get, detected));
            }
            stmt.close();
            conn.close();
            return tracks;
        }catch(SQLException sqlex)
        {
            _log.error(TAG + "getTrackByStation() StackTrace: "+sqlex.getStackTrace()+" Message : "+sqlex.getMessage());
            return null;
        }
    }

    @Override
    public LinkedList<Track> getTrackByHikerAtStation(String tag, String station)
    {
        try {
            conn = DriverManager.getConnection("jdbc:sqlite:mountainrescue.db");
            Statement stmt = conn.createStatement();
            String sql ="SELECT * FROM HIKER " +
                    "WHERE TAG = '"+tag+"' AND STATION = '"+station+"';";
            ResultSet rs = stmt.executeQuery(sql);
            LinkedList<Track> tracks = new LinkedList<>();
            while ( rs.next() ) {
                String tag_get = rs.getString("TAG");
                String station_get = rs.getString("STATION");
                Date detected = new Date(rs.getLong("TIME") * 60 *1000);

                tracks.add(new Track(tag_get, station_get, detected));
            }
            stmt.close();
            conn.close();
            return tracks;
        }catch(SQLException sqlex)
        {
            _log.error(TAG + "getTrackByHikerAtStation() StackTrace: "+sqlex.getStackTrace()+" Message : "+sqlex.getMessage());
            return null;
        }
    }

    @Override
    public boolean saveTrack(Track track)
    {
        try {
            conn = DriverManager.getConnection("jdbc:sqlite:mountainrescue.db");
            Statement stmt = conn.createStatement();
            Long date = track.detected.getTime()/60000;
            String sql ="INSERT INTO HIKER (TAG, STATION, TIME) "+
                    "VALUES ('"+track.hiker.toString()+"', '"+track.station+"', "+date+");";
            stmt.executeUpdate(sql);
            stmt.close();
            conn.close();
            return true;
        }catch(SQLException sqlex)
        {
            _log.error(TAG + "saveTrack() StackTrace: "+sqlex.getStackTrace()+" Message : "+sqlex.getMessage());
            return false;
        }
    }

    /*saves the track only if
    * its from the current station and newer by save_interval. save_interval is defined in config.properties
    * OR
    * its from a different station and is a new entry*/
    @Override
    public boolean saveNewTrack(Track track) {
        try {
            conn = DriverManager.getConnection("jdbc:sqlite:mountainrescue.db");
            Statement stmt = conn.createStatement();
            Long date = track.detected.getTime()/60000;

            /*Search for record*/
            String search = "SELECT (count(*) > 0) as found " +
                    "FROM HIKER WHERE TAG = '"+track.hiker.toString()+"' AND STATION = '"+track.station+"' AND TIME = "+date+";";
            ResultSet rs = stmt.executeQuery(search);
            if(rs.next())
            {
                boolean found = rs.getBoolean(1);
                if(!found)
                {
                    String sql ="INSERT INTO HIKER (TAG, STATION, TIME) "+
                            "VALUES ('"+track.hiker.toString()+"', '"+track.station+"', "+date+");";
                    stmt.executeUpdate(sql);
                    _log.debug(TAG + "saveNewTrack() inserted new record "+track.toString());
                }
                else
                {
                    _log.debug(TAG + "saveNewTrack() not inserted. Record already present");
                }
            }


            stmt.close();
            conn.close();
            return true;
        }catch(SQLException sqlex)
        {
            _log.error(TAG + "saveNewTrack() StackTrace: "+sqlex.getStackTrace()+" Message : "+sqlex.getMessage());
            return false;
        }
    }

    public boolean saveIfNewerTrack(Track track, Date pastDate)
    {
        try
        {
            conn = DriverManager.getConnection("jdbc:sqlite:mountainrescue.db");
            Statement stmt = conn.createStatement();
            Long date = track.detected.getTime()/60000;
            Long pastDate_value = pastDate.getTime()/60000;
            //Long timeRange = pastDate_value - (Util.getSaveInterval() / 60);

            /*Search for record*/
            String search = "SELECT (count(*) > 0) as found " +
                    "FROM HIKER WHERE TAG = '"+track.hiker.toString()+"' AND STATION = '"+track.station+"' AND TIME > "+pastDate_value+";";
            ResultSet rs = stmt.executeQuery(search);
            if(rs.next())
            {
                boolean found = rs.getBoolean(1);
                if(!found)
                {
                    String sql ="INSERT INTO HIKER (TAG, STATION, TIME) "+
                            "VALUES ('"+track.hiker.toString()+"', '"+track.station+"', "+date+");";
                    stmt.executeUpdate(sql);
                    _log.debug(TAG + "saveIfNewerTrack() inserted new record "+track.toString());
                }
                else
                {
                    _log.debug(TAG + "saveIfNewerTrack() not inserted. Record already present");
                }
            }


            stmt.close();
            conn.close();
            return true;
        }catch(SQLException sqlex)
        {
            _log.error(TAG + "saveIfNewerTrack() StackTrace: "+sqlex.getStackTrace()+" Message : "+sqlex.getMessage());
            return false;
        }
    }

    @Override
    public LinkedList<SOS> getAllSOS() {
        try {
            conn = DriverManager.getConnection("jdbc:sqlite:mountainrescue.db");
            Statement stmt = conn.createStatement();
            String sql ="SELECT * FROM SOS ;";
            ResultSet rs = stmt.executeQuery(sql);
            LinkedList<SOS> soses = new LinkedList<>();
            while ( rs.next() ) {
                String tag = rs.getString("TAG");
                String station = rs.getString("STATION");
                Date detected = new Date(rs.getLong("TIME") * 60 * 1000);
                String message = rs.getString("MESSAGE");

                soses.add(new SOS(tag, station, detected, message));
            }
            stmt.close();
            conn.close();
            return soses;
        }catch(SQLException sqlex)
        {
            _log.error(TAG + "getAllSOS() StackTrace: "+sqlex.getStackTrace()+" Message : "+sqlex.getMessage());
            return null;
        }
    }

    @Override
    public LinkedList<SOS> getSOSbyStation(String station) {
        try {
            conn = DriverManager.getConnection("jdbc:sqlite:mountainrescue.db");
            Statement stmt = conn.createStatement();
            String sql ="SELECT * FROM SOS " +
                    "WHERE STATION = '"+station+"';";
            ResultSet rs = stmt.executeQuery(sql);
            LinkedList<SOS> soses = new LinkedList<>();
            while ( rs.next() ) {
                String tag = rs.getString("TAG");
                String station_get = rs.getString("STATION");
                Date detected = new Date(rs.getLong("TIME") *60 *1000);
                String message = rs.getString("MESSAGE");

                soses.add(new SOS(tag, station_get, detected, message));
            }
            stmt.close();
            conn.close();
            return soses;
        }catch(SQLException sqlex)
        {
            _log.error(TAG + "getSOSbyStation() StackTrace: "+sqlex.getStackTrace()+" Message : "+sqlex.getMessage());
            return null;
        }
    }

    /*save sos only if information not already present*/
    @Override
    public boolean saveNewSOS(SOS sos) {
        try {
            conn = DriverManager.getConnection("jdbc:sqlite:mountainrescue.db");
            Statement stmt = conn.createStatement();
            Long date = sos.detected.getTime()/60000;

            /*search for a similar sos*/
            String search = "SELECT (count(*) > 0) as found FROM SOS WHERE TAG = '"+sos.hiker.toString()+"' AND STATION = '"+sos.station+"' AND TIME = "+date+" AND MESSAGE ='"+sos.message+"';";
            ResultSet rs = stmt.executeQuery(search);

            if(rs.next()){
                boolean found = rs.getBoolean(1);
                if(!found)
                {
                    /*save if not found*/
                    String insert ="INSERT INTO SOS (TAG, STATION, TIME, MESSAGE) "+
                            "VALUES ('"+sos.hiker.toString()+"', '"+sos.station+"', "+date+", '"+sos.message+"');";
                    stmt.executeUpdate(insert);
                    _log.debug(TAG + "saveNewSOS() inserted new record "+sos.toString());
                }
                else
                {
                    _log.debug(TAG + "saveNewSOS() not inserted. Record already present");
                }
            }

            stmt.close();
            conn.close();
            return true;
        }catch(SQLException sqlex)
        {
            _log.error(TAG + "getTrackByTimeRange() StackTrace: "+sqlex.getStackTrace()+" Message : "+sqlex.getMessage());
            return false;
        }
    }
}
