package de.tum.in.mountain_rescue.station;

import de.tum.in.mountain_rescue.station.packet.GetMapResponse;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * Created by rabbiddog on 15.09.16.
 */
public class Util {

    private static String service_name = null;
    private static String station_Id = null;
    private static int save_interval = 0;
    private Util(){}
    public static String byteArrayToHex( final byte[] a ) {
        StringBuilder sb = new StringBuilder( a.length * 2 );
        for ( final byte b : a )
            sb.append( String.format( "%02x", b & 0xff ) );
        return sb.toString();
    }

    public static String getServiceName()
    {
        if(null != service_name)
        {
            return service_name;
        }
        File configFile = new File("config.properties");

        try {
            FileReader reader = new FileReader(configFile);
            Properties props = new Properties();
            props.load(reader);
            service_name= props.getProperty("service");
            reader.close();
            return service_name;
        } catch (FileNotFoundException ex) {
            System.out.print("Error in reading Configuration file while searching for path to Log file");
            return null;
        } catch (IOException ex) {
            System.out.print("Error in reading Configuration file while searching for path to Log file");
            return null;
        }
    }

    public static String getStationId()
    {
        if(null != station_Id)
            return station_Id;
        File configFile = new File("config.properties");

        try {
            FileReader reader = new FileReader(configFile);
            Properties props = new Properties();
            props.load(reader);
            station_Id= props.getProperty("stationId");
            reader.close();
            return station_Id;
        } catch (FileNotFoundException ex) {
            System.out.print("Error in reading Configuration file while searching for path to Log file");
            return null;
        } catch (IOException ex) {
            System.out.print("Error in reading Configuration file while searching for path to Log file");
            return null;
        }
    }

    public static int getSaveInterval()
    {
        if(0 != save_interval)
            return save_interval;
        File configFile = new File("config.properties");

        try {
            FileReader reader = new FileReader(configFile);
            Properties props = new Properties();
            props.load(reader);
            save_interval= Integer.parseInt( props.getProperty("save_interval"));
            reader.close();
            return save_interval;
        } catch (FileNotFoundException ex) {
            System.out.print("Error in reading Configuration file while searching for path to Log file");
            return 15;
        } catch (IOException ex) {
            System.out.print("Error in reading Configuration file while searching for path to Log file");
            return 15;
        }
    }

    /*removes the jar name from the path so that the path referes to where the jar is installer*/
    public static String filterJarFilePath(String path)
    {
        String filtered = path;
        int idxFile = path.lastIndexOf('/');

        if(idxFile != -1)
        {
            filtered = path.substring(0, idxFile);
        }

        return filtered;
    }

    public static byte[] getLocalMap()
    {
        try
        {
            String s = Util.getStationId();
            String path_jar = Util.filterJarFilePath(Main.class.getProtectionDomain().getCodeSource().getLocation().getFile());
            //URL mapPath = Main.class.getClassLoader().getResource(s+".zip");
            byte[] map  = java.nio.file.Files.readAllBytes(Paths.get(path_jar, s+".zip"));
            if(null == map)
                System.out.print("Class : Util :- map byte is null : ");

            return map;
        }catch (Exception ex)
        {
            System.out.print("Class : Util :- Error while getting local map. StackTrace : "+ex.getStackTrace()+" Messgae : "+ex.getMessage());
            return null;
        }
    }
}
