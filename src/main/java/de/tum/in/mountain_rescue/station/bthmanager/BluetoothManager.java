package de.tum.in.mountain_rescue.station.bthmanager;

import de.tum.in.mountain_rescue.station.*;
import de.tum.in.mountain_rescue.station.eddystone.Parse;
import de.tum.in.mountain_rescue.station.eddystone.frames.EddystoneFrame;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import sun.util.resources.cldr.zh.LocaleNames_zh_Hans_MO;

import java.io.*;
import java.net.URL;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by rabbiddog on 16.09.16.
 */
public class BluetoothManager extends service{

    private static BluetoothManager _instance;
    private static Logger _log;
    private final static String TAG = "Class : BluetoothManager :-";

    /*instance properties*/
    private AtomicBoolean scan_running;
    private ProcessBuilder start_hcitool_lescan;
    private ProcessBuilder start_hcidump_raw;
    private ProcessBuilder stop_hcitool_lescan;
    private ProcessBuilder stop_hcidump_raw;
    private ProcessBuilder read_raw_dump;
    private String path_hcidump_dump;

    public ConcurrentLinkedQueue<EddystoneFrame> getFrameQueue() {
        return frameQueue;
    }

    private ConcurrentLinkedQueue<EddystoneFrame> frameQueue;
    private ExecutorService service;

    private BluetoothManager()
    {
        _log = LogManager.getRootLogger();
        scan_running = new AtomicBoolean(false);
        frameQueue = new ConcurrentLinkedQueue<>();
        String path_jar = Util.filterJarFilePath(getClass().getProtectionDomain().getCodeSource().getLocation().getFile());

        String path_scan_ble = path_jar+"/scan_ble.sh";
        _log.debug(TAG + "constructor path to scan_ble " + path_scan_ble);

        String path_hcidump = path_jar+"/hcidump.sh";
        _log.debug(TAG + "constructor path to hcidump " + path_hcidump);
        path_hcidump_dump = path_jar+"/hcidump_dump";
        _log.debug(TAG + "constructor path to hcidump_dump " + path_hcidump_dump);

        String path_stop_scan_ble = path_jar+"stop_scan_ble.sh";
        _log.debug(TAG + "constructor path to stop_scan_ble "+path_stop_scan_ble);

        String path_stop_hcidump = path_jar+"/stop_hcidump_dump.sh";
        _log.debug(TAG + "constructor path to stop_hcidump "+path_stop_hcidump);

        String path_read_hcidump = path_jar + "/read_hcidump.sh";
        _log.debug(TAG + "constructor path to read_hcidump "+path_read_hcidump);

        start_hcitool_lescan = new ProcessBuilder("/bin/sh", path_scan_ble);
        start_hcitool_lescan.redirectErrorStream(true);
        start_hcidump_raw = new ProcessBuilder("/bin/sh", path_hcidump);
        start_hcidump_raw.redirectErrorStream(true);
        stop_hcitool_lescan = new ProcessBuilder("/bin/sh", path_stop_scan_ble);
        stop_hcitool_lescan.redirectErrorStream(true);
        stop_hcidump_raw = new ProcessBuilder("/bin/sh", path_stop_hcidump);
        stop_hcidump_raw.redirectErrorStream(true);
        read_raw_dump = new ProcessBuilder("/bin/sh", path_read_hcidump).redirectInput(ProcessBuilder.Redirect.INHERIT).redirectErrorStream(true);

        service = Executors.newFixedThreadPool(1);
    }

    public static BluetoothManager getInstance()
    {
        if(null == _instance)
        {
            _instance = new BluetoothManager();
        }
        return _instance;
    }

    public boolean start()
    {
        if(scan_running.get())
        {
            _log.debug(TAG + "start() service already running");
            return true;
        }
        try {
            _log.debug(TAG +"startScan() will start hcitool_lescan");

            start_hcitool_lescan.start();

            _log.debug(TAG +"startScan() start_hcitool_lescan executed");
            _log.debug(TAG +"startScan() will start hcidump_raw");
            start_hcidump_raw.start();
            _log.debug(TAG +"startScan() start_hcidump_raw executed");
            Thread.sleep(200);
            service.submit(new Thread(){
                public void run(){
                   getBeaconFrame();
                }
            });
            _log.debug(TAG + "start() service started");
            scan_running.set(true);
            return true;
        }
        catch (IOException ioex)
        {
            _log.error(TAG + "startScan() error starting bluetooth packet scan. StackTrace: "+ ioex.getStackTrace() +" Message: "+ioex.getMessage());
            return false;
        }
        catch (Exception ex)
        {
            _log.error(TAG + "startScan() error starting bluetooth packet scan. StackTrace: "+ ex.getStackTrace() +" Message: "+ex.getMessage());
           return false;
        }
    }

    public boolean stop()
    {
        if(scan_running.get())
        {
            try
            {
                stop_hcidump_raw.start();
                scan_running.set(false);
                stop_hcitool_lescan.start();
                service.shutdownNow();
                _log.debug(TAG + "stop() awaiting termination");
                service.awaitTermination(20, TimeUnit.SECONDS);
                _log.debug(TAG + "stop() process stopped");
                scan_running.set(false);
                return true;
            }catch (IOException ioex)
            {
                _log.error(TAG +"stopScan() error stopping scan. StackTrace: "+ ioex.getStackTrace()+ " Message: "+ ioex.getMessage());
                return false;
            }catch (Exception ex)
            {
                _log.error(TAG + "stopScan() error stopping scan. StackTrace: "+ ex.getStackTrace()+ " Message: "+ ex.getMessage());
                return false;
            }
        }
        else
        {
            _log.debug(TAG + "stop() service was not running");
            return true;
        }

    }

    /*parses through the dump file to find and send back only eddystone frames*/
    private void getBeaconFrame()
    {
        _log.debug(TAG +"getBeaconFrame() start thread");
        try
        {
            RandomAccessFile pipe = new RandomAccessFile("/tmp/hcidump_data", "r");
            StringBuilder builder = new StringBuilder();
            while (scan_running.get())
            {
                if(Thread.currentThread().isInterrupted())
                {
                    _log.debug(TAG + "getBeaconFrame() thread was interrupted. Will shut down now");
                    break;
                }

                builder.append(pipe.readLine());
                String line = getLine(builder);
                if(null != line)
                {
                    line = filterBeacon(line);
                    _log.debug(TAG + "getBeaconFrame() data found in hcidump : " + line);
                    EddystoneFrame frame = Parse.getInstance().parseData(line);
                    if(null != frame)
                    {
                        _log.debug(TAG + "getBeaconFrame() frame recovered from dump "+frame.toString());
                        this.frameQueue.offer(frame);
                    }
                    else
                    {
                        _log.debug(TAG + "getBeaconFrame() parser returned null frame");
                    }
                }
                else
                {
                    _log.debug(TAG + "getBeaconFrame() no data found in hcidump");
                    Thread.sleep(100);
                }
            }
            _log.debug(TAG +"getBeaconFrame() has ended");

        }
        catch(FileNotFoundException fex)
        {
            _log.error(TAG + "getBeaconFrame() error. StackTrace: "+ fex.getStackTrace()+ " Message: "+ fex.getMessage());
            stop();
        }
        catch (IOException ioex)
        {
            _log.error(TAG + "getBeaconFrame() error. StackTrace: "+ ioex.getStackTrace()+ " Message: "+ ioex.getMessage());
            stop();
        }catch (Exception ex)
        {
            _log.error(TAG + "getBeaconFrame() error. StackTrace: "+ ex.getStackTrace()+ " Message: "+ ex.getMessage());
            stop();
        }
    }



    private String filterBeacon(String b)
    {
        b = b.replace(System.getProperty("line.separator").toString(), "");
        b = b.replace("\r", "");
        b = b.replace(">" , "");
        b = b.trim().replaceAll("\\s{2,}", " ");
        return b;
    }
    private String getLine(StringBuilder builder)
    {
        int start = builder.indexOf(">");
        if(start == -1)
        {
            return null;
        }
        else
        {
            int end = builder.indexOf(">", start+1);
            if(end == -1)
            {
                return null;
            }
            else
            {
                String s = builder.substring(start, end);
                builder.delete(start, end);
                return s;
            }
        }
    }
}
