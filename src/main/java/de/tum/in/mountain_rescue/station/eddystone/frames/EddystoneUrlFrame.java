package de.tum.in.mountain_rescue.station.eddystone.frames;

/**
 * Class for parsing Eddystone-URL frames.
 *
 * @author teemuk
 */
public final class EddystoneUrlFrame extends EddystoneFrame{

  //==============================================================================================//
  // Constants
  //==============================================================================================//
  private static String[] PREFIX_ENCODINGS
      = { "http://www.", "https://www.", "http://", "https://" };
  private static String[] URL_ENCODINGS
      = { ".com/", ".org/", ".edu/", ".net/", ".info/", ".biz/", ".gov/",
          ".com", ".org", ".edu", ".net", ".info", ".biz", ".gov" };
  //==============================================================================================//

  //==============================================================================================//
  // Instance vars
  //==============================================================================================//
  public final int txPower_dBm;
  public final String url;
  //==============================================================================================//


  //==============================================================================================//
  // API
  //==============================================================================================//
  public EddystoneUrlFrame(
      final int txPower_dBm,
      final String url ) {
    this.txPower_dBm = txPower_dBm;
    this.url = url;
  }

  /**
   * Parses an Eddystone-URL frame from the given buffer.
   *
   * @param frameBytes
   *  Service Data bytes associated with the Eddystone service UUID.
   */
  public static EddystoneUrlFrame parse( final byte[] frameBytes ) {
    // Validate length. Must be at least 3 bytes.
    // Accept URLs consisting only of a prefix, although the spec seems to assume at least
    // prefix + 1 character (being liberal with what we accept).
    if ( frameBytes.length < 3 ) {
      return null;
    }

    // Validate type
    if ( !isUrlFrame( frameBytes ) ) {
      return null;
    }

    // TX Power
    final int txPower = frameBytes[ 1 ];

    // URL Prefix
    final int prefixIndex = frameBytes[ 2 ] & 0xff;
    if ( prefixIndex >= PREFIX_ENCODINGS.length ) {
      return null;
    }
    final String urlPrefix = PREFIX_ENCODINGS[ prefixIndex ];

    // Encoded url
    String urlPostfix = "";
    for ( int i = 3; i < frameBytes.length; i++ ) {
      urlPostfix += decodeUrlChar( frameBytes, i );
    }

    return new EddystoneUrlFrame( txPower, urlPrefix + urlPostfix );
  }

  /**
   * Checks that the given frame is Eddystone-URL frame. The top 4 bits must be 0001.
   *
   * @param bytes
   *  Raw frame data.
   * @return
   *  {@code true} if the frame type of the buffer matches Eddystone-URL {@code false} otherwise.
   */
  public static boolean isUrlFrame( final byte[] bytes ) {
    return ( ( ( bytes[ 0 ] & 0xff ) >> 4 ) == 1 );
  }

  @Override
  public int geType() {
    return FrameType.URL;
  }

  @Override
  public String toString() {
    return "URL = " + this.url + ", txPower = " + this.txPower_dBm + "dBm";
  }
  //==============================================================================================//


  //==============================================================================================//
  // Private
  //==============================================================================================//
  private static String decodeUrlChar(
      final byte[] bytes,
      final int position ) {
    final int val = bytes[ position ] & 0xff;

    if ( val <= 13 ) {
      return URL_ENCODINGS[ val ];
    } else if ( val <= 32 ) {
      return "";
    } else if ( val >= 127 ) {
      return "";
    } else {
      return Character.toString( ( char ) val );
    }
  }
  //==============================================================================================//
}
