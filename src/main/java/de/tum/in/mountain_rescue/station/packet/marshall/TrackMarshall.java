package de.tum.in.mountain_rescue.station.packet.marshall;

/**
 * Created by rabbiddog on 21.09.16.
 */
import de.tum.in.mountain_rescue.station.packet.PacketType;
import de.tum.in.mountain_rescue.station.packet.Track;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.PooledByteBufAllocator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.ParseException;
import java.util.Date;
import java.text.SimpleDateFormat;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

public class TrackMarshall {

    private static final PooledByteBufAllocator ALLOCATOR =
            PooledByteBufAllocator.DEFAULT;
    private static final int TYPE = 1;
    private static final int HIKER = 32;
    private static final int STATION = 30;
    private static final int DATE = 19;
    private static final byte type_value = PacketType.TRACK;
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static Logger _log = LogManager.getRootLogger();
    private final static String TAG = "Class : TrackMarshall :-";

    private static final int LENGTH = TYPE + HIKER + STATION + DATE;

    public static Track decode(byte[] data)
    {
        checkNotNull(data);
        checkArgument(data.length == LENGTH);

        ByteBuf buffer = null;
        try
        {
            buffer = ALLOCATOR.buffer(LENGTH);
            buffer.writeBytes(data);
            byte type = buffer.readByte();
            checkState(type == type_value);

            /*get hiker tag*/
            char[] hiker_value = new char[HIKER];
            for(int i = 0 ; i<HIKER ; i++)
            {
                byte v = buffer.readByte();
                hiker_value[i] = (char)v;
            }

            /*get station*/
            char[] station_value = new char[STATION];
            for(int i  = 0; i< STATION; i++)
            {
                byte v = buffer.readByte();
                station_value[i] = (char)v;
            }
            /*get date*/
            char[] date_value = new char[DATE];
            for(int i = 0; i<DATE ; i++)
            {
                byte v =buffer.readByte();
                date_value[i] = (char)v;
            }

            try {
                Date d = dateFormat.parse(new String(date_value));
                Track t = new Track(new String(hiker_value), new String(station_value), d);
                return t;
            }catch (ParseException pex)
            {
                _log.error(TAG + " decode() date format parse failed." + new String(date_value));
                return null;
            }

        }finally {
            if (buffer != null) {
                buffer.release();
            }
        }
    }

    public static byte[] encode(Track track)
    {
        checkNotNull(track);
        ByteBuf buffer = null;
        try {
            buffer = ALLOCATOR.buffer(LENGTH);
            buffer.writeByte(track.getType());
            /*decompile hiker*/
            String hiker_value =  track.hiker.toString();
            for(int i = 0; i< HIKER; i++)
            {
                buffer.writeByte(hiker_value.charAt(i));
            }
            /*decompile station*/
            for(int i = 0; i< STATION; i++)
            {
                buffer.writeByte(track.station.charAt(i));
            }
            /*decompile detected*/
            String detected_value = dateFormat.format(track.detected);
            for(int i = 0; i< DATE; i++)
            {
                buffer.writeByte(detected_value.charAt(i));
            }

            byte[] data = new byte[LENGTH];
            buffer.readBytes(data);

            return data;
        }finally {
            if (buffer != null) {
                buffer.release();
            }
        }
    }
}
