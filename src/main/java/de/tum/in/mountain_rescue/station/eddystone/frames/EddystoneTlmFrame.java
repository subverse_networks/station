package de.tum.in.mountain_rescue.station.eddystone.frames;

/**
 * Class for parsing Eddystone-TLM frames.
 *
 * @author teemuk
 */
public final class EddystoneTlmFrame extends EddystoneFrame{

  //==============================================================================================//
  // Constants
  //==============================================================================================//

  //==============================================================================================//

  //==============================================================================================//
  // Instance vars
  //==============================================================================================//
  public final short version;
  /** Battery voltage in millivolts. */
  public final int batteryVoltage_mV;
  /** Tempterature of the beacon or {@code NaN} if not supported. */
  public final double temperature_C;
  /** Number of advertisement frames sent since boot. */
  public final long advertCount;
  /** Time since boot in deciseconds (0.1s). */
  public final long uptime_ds;
  //==============================================================================================//


  //==============================================================================================//
  // API
  //==============================================================================================//
  public EddystoneTlmFrame(
      final short version,
      final int batteryVoltage_mV,
      final double temperature_C,
      final long advertCount,
      final long uptime_ds ) {
    this.version = version;
    this.batteryVoltage_mV = batteryVoltage_mV;
    this.temperature_C = temperature_C;
    this.advertCount = advertCount;
    this.uptime_ds = uptime_ds ;
  }

  /**
   * Parses an Eddystone-TLM frame from the given buffer.
   *
   * @param frameBytes
   *  Service Data bytes associated with the Eddystone service UUID.
   */
  public static EddystoneTlmFrame parse( final byte[] frameBytes ) {
    // Validate length. TLM frame is exactly 14 bytes, but accept larger buffers.
    if ( frameBytes.length < 14 ) {
      return null;
    }

    // Validate type
    if ( !isTlmFrame( frameBytes ) ) {
      return null;
    }

    // Version
    final short version = frameBytes[ 1 ];

    // Battery level
    final int batteryVoltage = read2Bytes( frameBytes, 2 );

    // Temperature
    final double temperature = read88fp( frameBytes, 4 );

    // Advert count
    final long advertCount = read4Bytes( frameBytes, 6 );

    // Uptime deciseconds
    final long uptime = read4Bytes( frameBytes, 10 );

    return new EddystoneTlmFrame( version, batteryVoltage, temperature, advertCount, uptime );
  }

  /**
   * Checks that the given frame is Eddystone-TLM frame. The top 4 bits must be 0010.
   *
   * @param bytes
   *  Raw frame data.
   * @return
   *  {@code true} if the frame type of the buffer matches Eddystone-TLM {@code false} otherwise.
   */
  public static boolean isTlmFrame( final byte[] bytes ) {
    return ( ( ( bytes[ 0 ] & 0xff ) >> 4 ) == 2 );
  }

  @Override
  public int geType() {
    return FrameType.TLM;
  }

  @Override
  public String toString() {
    return "version = " + this.version + ", battery = " + this.batteryVoltage_mV + "mV, "
        + "temperature = " + this.temperature_C + "C, beacon count = " + this.advertCount + ", "
        + "uptime = " + ( this.uptime_ds / 10.0 ) + "s";
  }
  //==============================================================================================//


  //==============================================================================================//
  // Private
  //==============================================================================================//
  private static int read2Bytes(
      final byte[] buffer,
      final int pos ) {
    return ( ( buffer[ pos     ] & 0xff ) << 8 ) |
           ( ( buffer[ pos + 1 ] & 0xff )      );
  }

  private static long read4Bytes(
      final byte[] buffer,
      final int pos ) {
    long val = ( buffer[ pos     ] & 0xff ) << 24;
        val |= ( buffer[ pos + 1 ] & 0xff ) << 16;
        val |= ( buffer[ pos + 2 ] & 0xff ) << 8;
        val |= ( buffer[ pos + 3 ] & 0xff );
    return val;
  }

  /** Reads 8:8 fixed point value. */
  private static double read88fp(
      final byte[] buffer,
      final int pos ) {
    final short fixed = ( short ) read2Bytes( buffer, pos );
    return ( ( double ) fixed ) / 256.0;
  }
  //==============================================================================================//
}
