package de.tum.in.mountain_rescue.station;

import de.tum.in.mountain_rescue.station.eddystone.frames.EddystoneFrame;

/**
 * Created by rabbiddog on 19.09.16.
 */
public interface IEddyStoneDetected {

    void processFrame(EddystoneFrame frame);
}
