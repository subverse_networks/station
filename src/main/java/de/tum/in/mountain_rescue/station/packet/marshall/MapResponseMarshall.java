package de.tum.in.mountain_rescue.station.packet.marshall;

import java.nio.file.*;
import de.tum.in.mountain_rescue.station.packet.GetMapResponse;
import de.tum.in.mountain_rescue.station.packet.PacketType;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.PooledByteBufAllocator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

/**
 * Created by rabbiddog on 26.09.16.
 */
public class MapResponseMarshall {

    private static final PooledByteBufAllocator ALLOCATOR =
            PooledByteBufAllocator.DEFAULT;
    private static final int TYPE = 1;
    private static final int STATION = 30;
    private static final int LENGTH = TYPE + STATION;
    private static final byte type_value = PacketType.MAPRESPONSE;
    private static Logger _log = LogManager.getRootLogger();
    private final static String TAG = "Class : MapResponseMarshall :-";

    public static GetMapResponse decode(byte[] data)
    {

        checkNotNull(data);
        ByteBuf buffer = null;
        try
        {
            buffer = ALLOCATOR.buffer(data.length);
            buffer.writeBytes(data);
            byte type = buffer.readByte();
            checkState(type == type_value);

            /*get station*/
            char[] station_value = new char[STATION];
            for(int i  = 0; i< STATION; i++)
            {
                byte v = buffer.readByte();
                station_value[i] = (char)v;
            }

            int map_data_len = data.length - LENGTH;
            byte[] map = new byte[map_data_len];
            buffer.readBytes(map);

            return new GetMapResponse(new String(station_value), map);

        }finally {
            if (buffer != null) {
                buffer.release();
            }
        }
    }

    public static byte[] encode(GetMapResponse rsp)
    {
        checkNotNull(rsp);
        ByteBuf buffer = null;
        try {
            buffer = ALLOCATOR.buffer(LENGTH + rsp.map.length);
            buffer.writeByte(rsp.getType());

            /*decompile station*/
            for(int i = 0; i< STATION; i++)
            {
                buffer.writeByte(rsp.station.charAt(i));
            }

            buffer.writeBytes(rsp.map);

            byte[] data = new byte[LENGTH + rsp.map.length];
            buffer.readBytes(data);

            return data;
        }finally {
            if (buffer != null) {
                buffer.release();
            }
        }
    }
}
