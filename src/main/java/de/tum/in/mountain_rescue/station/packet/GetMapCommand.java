package de.tum.in.mountain_rescue.station.packet;

/**
 * Created by rabbiddog on 26.09.16.
 */
public class GetMapCommand extends Packet {
    @Override
    public int getType() {
        return PacketType.MAPCOMMAND;
    }
}
