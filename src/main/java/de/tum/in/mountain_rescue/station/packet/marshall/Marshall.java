package de.tum.in.mountain_rescue.station.packet.marshall;

import de.tum.in.mountain_rescue.station.packet.*;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.PooledByteBufAllocator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

/**
 * Created by rabbiddog on 26.09.16.
 */
public class Marshall {

    private static final PooledByteBufAllocator ALLOCATOR =
            PooledByteBufAllocator.DEFAULT;
    private static final int TYPE = 1;

    private static Logger _log = LogManager.getRootLogger();
    private final static String TAG = "Class : Marshall :-";

    public static Packet decode(byte[] data)
    {
        checkNotNull(data);
        ByteBuf buffer = null;
        try
        {
            buffer = ALLOCATOR.buffer(data.length);
            buffer.writeBytes(data);
            byte type = buffer.readByte();

            if(PacketType.TRACK == type)
            {
                return TrackMarshall.decode(data);
            }
            else if(PacketType.MAPCOMMAND == type)
            {
                return MapCommandMarshall.decode(data);
            }
            else if(PacketType.MAPRESPONSE == type)
            {
                return MapResponseMarshall.decode(data);
            }
            else if(PacketType.SOS == type)
            {
                return SOSMarshall.decode(data);
            }
            else
            {
                _log.error(TAG + "decode() did not match any packet type. returning null");
                return null;
            }

        }finally {
            if (buffer != null) {
                buffer.release();
            }
        }
    }

    public static byte[] encode(Packet pck)
    {
        if(pck instanceof Track)
        {
            return TrackMarshall.encode((Track)pck);
        }
        else if(pck instanceof GetMapCommand)
        {
            return MapCommandMarshall.encode((GetMapCommand)pck);
        }
        else if(pck instanceof GetMapResponse)
        {
            return MapResponseMarshall.encode((GetMapResponse)pck);
        }
        else if(pck instanceof SOS)
        {
            return SOSMarshall.encode((SOS)pck);
        }
        else
        {
            _log.error(TAG + "decode() did not match any packet type. returning null");
            return null;
        }
    }
}
