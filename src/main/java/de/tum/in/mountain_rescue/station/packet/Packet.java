package de.tum.in.mountain_rescue.station.packet;

/**
 * Created by rabbiddog on 21.09.16.
 */
public abstract class Packet {


    public abstract int getType();
}
