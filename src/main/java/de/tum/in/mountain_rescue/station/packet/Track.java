package de.tum.in.mountain_rescue.station.packet;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by rabbiddog on 21.09.16.
 */
public class Track extends Packet {
    @Override
    public int getType() {
        return PacketType.TRACK;
    } //1 byte

    public Hiker hiker; //should be 32 char
    public String station; //should be 30 char.
    public Date detected; //"yyyy-MM-dd HH:mm:ss" 19 char //if hiker is detected in the same station after more than 15 min then record again
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public Track(String hiker, String station, Date detected)
    {
        this.hiker = new Hiker(hiker);
        this.station = station;
        this.detected = detected;
    }

    public Track(String uId, String instance, String station, Date detected)
    {
        this.hiker = new Hiker(uId, instance);
        this.station = station;
        this.detected = detected;
    }

    public String getDetectedTime()
    {
        return dateFormat.format(detected);
    }

    @Override
    public String toString()
    {
        return "Tag : "+hiker.toString()+", station : "+station+", detected : "+getDetectedTime();
    }
}
