package de.tum.in.mountain_rescue.station.eddystone.frames;

import de.tum.in.mountain_rescue.station.Util;

/**
 * Class for parsing Eddystone-UID frames.
 *
 * @author teemuk
 */
public final class EddystoneUidFrame extends EddystoneFrame{

  //==============================================================================================//
  // Constants
  //==============================================================================================//

  //==============================================================================================//

  //==============================================================================================//
  // Instance vars
  //==============================================================================================//
  /** Calibrated TX power at 0m. */
  public final int txPower_dBm;
  /** Namespace identifier bytes. */
  public final String namespace;
  /** Instance identifier bytes. */
  public final String instance;
  /** Reserved for future use. */
  public final int RSSI;
  //==============================================================================================//


  //==============================================================================================//
  // API
  //==============================================================================================//
  public EddystoneUidFrame(
      final int txPower_dBm,
      final String namespace,
      final String instance,
      final int RSSI ) {
    this.txPower_dBm = txPower_dBm;
    this.namespace = namespace;
    this.instance = instance;
    this.RSSI = RSSI;
  }

  /**
   * Parses an Eddystone-UID frame from the given buffer.
   *
   * @param frameBytes
   *  Service Data bytes associated with the Eddystone service UUID.
   */
  public static EddystoneUidFrame parse( final String[] frameBytes ) {
    // Validate length. Specified to exactly 20 bytes, but accept longer frames while ignoring
    // extra bytes.
    /*if ( frameBytes.length < 20 ) {
      return null;
    }*/

    // Validate type
    if ( !isUidFrame( frameBytes ) ) {
      return null;
    }

    // TX Power
    final int txPower = Integer.parseInt(frameBytes[ 26 ], 16) - 256;

    // Namespace
    final String[] namespace = new String[ 10 ];
    System.arraycopy( frameBytes, 27, namespace, 0, 10 );

    // Namespace)
    final String[] instance = new String[ 6 ];
    System.arraycopy( frameBytes, 37, instance, 0, 6 );

    final int rssi = Integer.parseInt(frameBytes[frameBytes.length-1], 16) -256;

    return new EddystoneUidFrame( txPower, String.join("", namespace), String.join("", instance), rssi );
  }

  /**
   * Checks that the given frame is Eddystone-UID frame. The top 4 bits must be 0000.
   *
   * @param hex
   *  Raw frame data in hex.
   * @return
   *  {@code true} if the frame type of the buffer matches Eddystone-UID {@code false} otherwise.
   */
  public static boolean isUidFrame( final String[] hex ) {
    //return ( ( ( bytes[ 0 ] & 0xff ) >> 4 ) == 0 );
    return hex[25].equals("00");
  }

  @Override
  public int geType() {
    return FrameType.UID;
  }

  @Override
  public String toString() {
    return "namespace = " + this.namespace + ", instance = "
        + this.instance  + ", txPower = " + this .txPower_dBm + "dBm";
  }
  //==============================================================================================//


  //==============================================================================================//
  // Private
  //==============================================================================================//

  //==============================================================================================//
}
