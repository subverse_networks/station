package de.tum.in.mountain_rescue.station;

import de.tum.in.mountain_rescue.station.bthmanager.BluetoothManager;
import de.tum.in.mountain_rescue.station.eddystone.frames.EddystoneFrame;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedList;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by rabbiddog on 19.09.16.
 */
public class DetectEddystoneFacade extends service {

    private static Logger _log;
    private AtomicBoolean _started;
    private static DetectEddystoneFacade _instance;
    private LinkedList<IEddyStoneDetected> frameSubscriber;
    private Object lock;
    private BluetoothManager bleManager;
    private ExecutorService service;
    private ConcurrentLinkedQueue<EddystoneFrame> frameQueue;
    private final static String TAG = "Class : DetectEddystoneFacade :-";

    public static DetectEddystoneFacade getInstance()
    {
        if(null == _instance)
        {
            _instance = new DetectEddystoneFacade();
        }
        return _instance;
    }

    private DetectEddystoneFacade()
    {
        _log = LogManager.getRootLogger();
        _started = new AtomicBoolean(false);
        frameSubscriber = new LinkedList<>();
        lock = new Object();
        bleManager = BluetoothManager.getInstance();
        frameQueue = BluetoothManager.getInstance().getFrameQueue(); //starts to write the raw results in a dump file

        service = Executors.newFixedThreadPool(1);
    }

    public void subscribe(IEddyStoneDetected sub)
    {
        synchronized(lock)
        {
            frameSubscriber.add(sub);
            _log.debug(TAG + "register() new subscriber added");
        }
    }

    public void deregister(IEddyStoneDetected sub)
    {
        synchronized(lock)
        {
            _log.debug(TAG + "deregister() subscriber removed");
            frameSubscriber.remove(sub);
        }
    }

    private void pushNotifications()
    {
        _log.debug(TAG +"pushNotifications() starting");
        while (true)
        {
            if(Thread.currentThread().isInterrupted())
            {
                _log.debug(TAG + "pushNotifications() was interrupted. Will stop now");
                break;
            }
            EddystoneFrame f = frameQueue.poll();
            if(null != f)
            {
                for (IEddyStoneDetected subscriber:frameSubscriber)
                {
                    subscriber.processFrame(f); //bad design. subscriber can hold up the execution of this thread. Refactor later
                }
            }
        }
        _log.debug(TAG + "pushNotifications() has ended");
    }

    @Override
    public boolean start() {
        if(_started.get())
        {
            _log.debug(TAG + "start() service already running");
            return true;
        }
        try{
            _log.debug(TAG + " service started");
            BluetoothManager.getInstance().start();
            service.submit(new Thread(){
                public void run(){
                    pushNotifications();
                }
            });
            _started.set(true);
            return true;
        }catch (Exception ex)
        {
            _log.error(TAG + "start() StackTrace : "+ex.getStackTrace()+ " Message : "+ex.getMessage());
            return false;
        }
    }

    @Override
    public boolean stop()
    {
        if(!_started.get())
        {
            _log.debug(TAG + "stop() service has stopped");
            return true;
        }
        boolean status = bleManager.stop();
        service.shutdownNow();
        _started.set(false);
        try{
            _log.debug(TAG + "stop() awaiting termination");
            service.awaitTermination(20, TimeUnit.SECONDS);
            _log.debug(TAG +"stop() process stopped");
            return status; //since bluetooth manager might not shutdown properly
        }catch (Exception ex)
        {
            _log.error(TAG + "stop() StackTrace : "+ex.getStackTrace()+ " Message : "+ex.getMessage());
            return false;
        }
    }
}
