package de.tum.in.mountain_rescue.station.packet.marshall;

import de.tum.in.mountain_rescue.station.packet.PacketType;
import de.tum.in.mountain_rescue.station.packet.SOS;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.PooledByteBufAllocator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

/**
 * Created by rabbiddog on 23.09.16.
 */
public class SOSMarshall {

    private static final PooledByteBufAllocator ALLOCATOR =
            PooledByteBufAllocator.DEFAULT;
    private static final int TYPE = 1;
    private static final int HIKER = 32;
    private static final int STATION = 30;
    private static final int DATE = 19;
    private static final byte type_value = PacketType.SOS;
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static Logger _log = LogManager.getRootLogger();
    private final static String TAG = "Class : SOSMarshall :-";

    private static final int LENGTH = TYPE + HIKER + STATION + DATE;

    public static SOS decode(byte[] data)
    {
        checkNotNull(data);
        checkArgument(data.length > LENGTH); //additional message length

        ByteBuf buffer = null;
        try
        {
            buffer = ALLOCATOR.buffer(LENGTH);
            buffer.writeBytes(data);
            byte type = buffer.readByte();
            checkState(type == type_value);

            /*get hiker tag*/
            char[] hiker_value = new char[HIKER];
            for(int i = 0 ; i<HIKER ; i++)
            {
                byte v = buffer.readByte();
                hiker_value[i] = (char)v;
            }

            /*get station*/
            char[] station_value = new char[STATION];
            for(int i  = 0; i< STATION; i++)
            {
                byte v = buffer.readByte();
                station_value[i] = (char)v;
            }
            /*get date*/
            char[] date_value = new char[DATE];
            for(int i = 0; i<DATE ; i++)
            {
                byte v =buffer.readByte();
                date_value[i] = (char)v;
            }

            int messageLen = data.length - LENGTH;
            char[] message = new char[messageLen];
            for(int i = 0; i < messageLen; i++)
            {
                byte v =buffer.readByte();
                message[i] = (char)v;
            }

            try {
                Date d = dateFormat.parse(new String(date_value));
                SOS sos = new SOS(new String(hiker_value), new String(station_value), d, new String(message));
                return sos;
            }catch (ParseException pex)
            {
                _log.error(TAG + " decode() date format parse failed." + new String(date_value));
                return null;
            }

        }finally {
            if (buffer != null) {
                buffer.release();
            }
        }
    }

    public static byte[] encode(SOS sos)
    {
        checkNotNull(sos);
        ByteBuf buffer = null;
        try {
            buffer = ALLOCATOR.buffer(LENGTH  + sos.message.length());
            buffer.writeByte(sos.getType());
            /*decompile hiker*/
            String hiker_value =  sos.hiker.toString();
            for(int i = 0; i< HIKER; i++)
            {
                buffer.writeByte(hiker_value.charAt(i));
            }
            /*decompile station*/
            for(int i = 0; i< STATION; i++)
            {
                buffer.writeByte(sos.station.charAt(i));
            }
            /*decompile detected*/
            String detected_value = dateFormat.format(sos.detected);
            for(int i = 0; i< DATE; i++)
            {
                buffer.writeByte(detected_value.charAt(i));
            }

            for(int i = 0; i < sos.message.length(); i++)
            {
                buffer.writeByte(sos.message.charAt(i));
            }

            byte[] data = new byte[LENGTH + sos.message.length()];
            buffer.readBytes(data);

            return data;
        }finally {
            if (buffer != null) {
                buffer.release();
            }
        }
    }
}
