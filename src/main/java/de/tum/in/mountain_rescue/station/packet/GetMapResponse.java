package de.tum.in.mountain_rescue.station.packet;

import de.tum.in.mountain_rescue.station.Util;
import de.tum.in.mountain_rescue.station.packet.Packet;
import de.tum.in.mountain_rescue.station.packet.PacketType;

/**
 * Created by rabbiddog on 26.09.16.
 */
public class GetMapResponse extends Packet {
    @Override
    public int getType() {
        return PacketType.MAPRESPONSE;
    }

    public String station; //should be 30 char.
    public byte[] map; //file name should be station.zip

    public GetMapResponse(String station)
    {
        this.station = station;
    }

    public GetMapResponse(String station, byte[] map)
    {
        this.station = station;
        this.map = map;
    }

    public static GetMapResponse getLocalStationMap()
    {
        String s = Util.getStationId();
        byte[] map = Util.getLocalMap();
        if(null == map)
        {
            return null;
        }

        return new GetMapResponse(s, map);
    }
}
